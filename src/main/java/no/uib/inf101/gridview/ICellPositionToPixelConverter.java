package no.uib.inf101.gridview;
import no.uib.inf101.colorgrid.CellPosition;

import java.awt.geom.Rectangle2D;


public interface ICellPositionToPixelConverter {
public Rectangle2D getBoundsForCell(CellPosition pos);
}
    
