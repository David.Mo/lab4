package no.uib.inf101.gridview;


import no.uib.inf101.colorgrid.ColorGrid;
import no.uib.inf101.colorgrid.IColorGrid;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.CellColor;


import java.awt.Color;
import javax.swing.JFrame;
// fått en del hjelp av Dag Himle

public class Main {
  public static void main(String[] args) {
    IColorGrid grid = new ColorGrid(3, 4);
    grid.set(new CellPosition(0, 0), Color.red);
    grid.set(new CellPosition(0, 3), Color.blue);
    grid.set(new CellPosition(2, 0), Color.yellow);
    grid.set(new CellPosition(2, 3), Color.green);
    GridView canvas = new GridView(grid);
    JFrame frame = new JFrame();
    frame.setTitle("sug meg");
    frame.setContentPane(canvas);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);

    
  }
}
