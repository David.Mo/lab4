package no.uib.inf101.gridview;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

import java.awt.geom.Rectangle2D; 


public class CellPositionToPixelConverter implements ICellPositionToPixelConverter {
  Rectangle2D box;
  GridDimension gd;
  double margin;


public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
  this.box = box;
  this.gd = gd;
  this.margin = margin;
}



@Override
public Rectangle2D getBoundsForCell(CellPosition pos) {
  double cellheight = (box.getHeight() - (margin * (gd.rows() + 1))) / gd.rows();
  double cellWidth = (box.getWidth() - (margin * (gd.cols() + 1))) / gd.cols();
  double x = box.getX() + (margin * (pos.col() + 1)) + (cellWidth * pos.col());
  double y = box.getY() + (margin * (pos.row() + 1)) + (cellheight * pos.row());
  return new Rectangle2D.Double(x, y, cellWidth, cellheight);
}

}