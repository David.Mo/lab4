package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid {
    public List<List<CellColor>> grid;
    private int height;
    private int length;
    /**
     * @param height
     * @param length
     */
    public ColorGrid(int height, int length) {
        this.height = height;
        this.length = length;
        this.grid = new ArrayList<List<CellColor>>(height);
        for (int i = 0; i < height; i++) {
            this.grid.add(new ArrayList<CellColor>(length));
            for (int j = 0; j < length; j++) {
                this.grid.get(i).add(new CellColor(new CellPosition(i, j), null));
            }
        }
    }
    @Override
    public int rows() {
      // TODO Auto-generated method stub
      return height;
    }
  
    @Override
    public int cols() {
      // TODO Auto-generated method stub
      return length;
    }
  
    @Override
    public List<CellColor> getCells() {
        List<CellColor> cells = new ArrayList<CellColor>();
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < length; j++) {
            cells.add(grid.get(i).get(j));
            }
        }
        return cells;  
}

    
    @Override
    public Color get(CellPosition pos) { 
        return grid.get(pos.row()).get(pos.col()).color();
    }
    @Override
    public void set(CellPosition pos, Color color) {
        grid.get(pos.row()).set(pos.col(), new CellColor(pos, color));
        
        
    }
  }

